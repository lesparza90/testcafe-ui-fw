import dotenv from 'dotenv'
dotenv.config()

export const ERROR_MESSAGES = {
  INVALID_EMAIL: {
    ERROR: 'Invalid email address.'
  },

  INVALID_PASSWORD: {
    ERROR: 'Wrong email or password.'
  },

  EMPTY_PASSWORD: {
    ERROR: 'Blank password.'
  }
}
