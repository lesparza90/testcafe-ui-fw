import dotenv from 'dotenv'
dotenv.config()

export const CREDENTIALS = {
  VALID_USER: {
    USEREMAIL: process.env.TODOISTEMAIL,
    PASSWORD: process.env.TODOISTPASSWORD
  },

  INVALID_EMAIL: {
    USEREMAIL: 'kren.lnhotmail.com',
    PASSWORD: process.env.TODOISTPASSWORD
  },

  INVALID_PASSWORD: {
    USEREMAIL: process.env.TODOISTEMAIL,
    PASSWORD: 'invalidPass'
  },

  INVALID_CREDENTIALS: {
    USEREMAIL: 'karen@testing.com',
    PASSWORD: 'invalidPassword'
  }

}
