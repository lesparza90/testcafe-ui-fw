import { Selector, t } from 'testcafe'

class NavBarPage {
  constructor () {
    this.accountButton = Selector('.settings_avatar')
    this.userDetails = Selector('.user_menu_details_text')
  }

  async clickAccount () {
    await t
      .click(this.accountButton)
  }
}

export default new NavBarPage()
