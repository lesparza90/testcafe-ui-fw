import { Selector, t } from 'testcafe'

class LoginPage {
  constructor () {
    this.loginLink = Selector('a').withText('Log in')
    this.emailInput = Selector('#email')
    this.passwordInput = Selector('#password')
    this.loginButton = Selector('.sel_login')
    this.errorMessage = Selector('.error_msg')
    this.timeZonePopup = Selector('.iframe_holder')
    this.timeZoneButton = Selector('.timezone_button')
  }

  async login (email, password) {
    await t
      .click(this.loginLink)
      .typeText(this.emailInput, email)
    if (password) await t.typeText(this.passwordInput, password)
    await t.click(this.loginButton)
    if (await this.timeZoneButton.visible) { await t.click(this.timeZoneButton) }
  }
}

export default new LoginPage()
