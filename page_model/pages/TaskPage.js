import { Selector, t } from 'testcafe'

const faker = require('faker')

class TaskPage {
  constructor () {
    this.textInput = Selector('.public-DraftEditor-content')
    this.addTaskButton = Selector('.plus_add_button')
    this.submitTaskButton = Selector('.ist_button_red')
    this.taskName = Selector('.task_content')
  }

  async createTask (description) {
    await t
      .click(this.addTaskButton)
      .typeText(this.textInput, description)
      .click(this.submitTaskButton)
      .wait(1000)
  }

  async isTaskCreated (taskName) { // check task name is visible
    return this.taskName.withText(taskName).visible
  }

  async createMultipleTasks (numTasks) { // create N tasks and save them in an array. it returns the array
    const taskList = []
    await t.click(this.addTaskButton)
    for (let i = 0; i < numTasks; i++) {
      const taskName = faker.random.word()
      await t
        .typeText(this.textInput, taskName)
        .click(this.submitTaskButton)
        .wait(1000)
      taskList.push(taskName)
    }
    return taskList
  }
}

export default new TaskPage()
