import LoginPage from '../pages/LoginPage'
import { CREDENTIALS } from '../data/Credentials'
import TaskPage from '../pages/TaskPage'

const faker = require('faker')

fixture('Task creation test')

  .page`https://todoist.com/`

  .beforeEach(async (t) => {
    await LoginPage.login(CREDENTIALS.VALID_USER.USEREMAIL, CREDENTIALS.VALID_USER.PASSWORD)
  })

test('Create a Task', async (t) => {
  const taskName = faker.random.word()
  await TaskPage.createTask(taskName)
  await t.expect(TaskPage.isTaskCreated(taskName)).ok({ allowUnawaitedPromise: true })
})

test('Create multiple tasks', async (t) => {
  const addedTasks = await TaskPage.createMultipleTasks(10)
  addedTasks.forEach(async (task) => {
    await t.expect(TaskPage.isTaskCreated(task)).ok({ allowUnawaitedPromise: true })
  })
})
