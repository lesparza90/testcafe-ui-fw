import LoginPage from '../pages/LoginPage'
import { CREDENTIALS } from '../data/Credentials'
import { ERROR_MESSAGES } from '../data/ErrorMessages'
import NavBarPage from '../pages/NavBarPage'

fixture('Login test')

  .page`https://todoist.com/`

test('Login to Todoist page with valid credentials', async (t) => {
  await LoginPage.login(CREDENTIALS.VALID_USER.USEREMAIL, CREDENTIALS.VALID_USER.PASSWORD)
  await NavBarPage.clickAccount()

  await t.expect(NavBarPage.userDetails.innerText).contains(CREDENTIALS.VALID_USER.USEREMAIL)
})

test('User cannot login with invalid email', async (t) => {
  await LoginPage.login(CREDENTIALS.INVALID_EMAIL.USEREMAIL, CREDENTIALS.INVALID_EMAIL.PASSWORD)

  await t.expect(LoginPage.errorMessage.exists).ok()
  await t.expect(LoginPage.errorMessage.innerText).eql(ERROR_MESSAGES.INVALID_EMAIL.ERROR)
})

test('User cannot login with invalid password', async (t) => {
  await LoginPage.login(CREDENTIALS.INVALID_PASSWORD.USEREMAIL, CREDENTIALS.INVALID_PASSWORD.PASSWORD)

  await t.expect(LoginPage.errorMessage.exists).ok()
  await t.expect(LoginPage.errorMessage.innerText).eql(ERROR_MESSAGES.INVALID_PASSWORD.ERROR)
})

test('User cannot login with empty password', async (t) => {
  await LoginPage.login(CREDENTIALS.VALID_USER.USEREMAIL, '')

  await t.expect(LoginPage.errorMessage.exists).ok()
  await t.expect(LoginPage.errorMessage.innerText).eql(ERROR_MESSAGES.EMPTY_PASSWORD.ERROR)
})

test('User cannot login with invalid email and password', async (t) => {
  await LoginPage.login(CREDENTIALS.INVALID_CREDENTIALS.USEREMAIL, CREDENTIALS.INVALID_CREDENTIALS.PASSWORD)

  await t.expect(LoginPage.errorMessage.exists).ok()
  await t.expect(LoginPage.errorMessage.innerText).eql(ERROR_MESSAGES.INVALID_PASSWORD.ERROR)
})
